# IDS 721 Individual Project 1 [![pipeline status](https://gitlab.com/jaxonyue/ids-721-individual-proj-1/badges/main/pipeline.svg)](https://gitlab.com/jaxonyue/ids-721-individual-proj-1/-/commits/main)

## Link to Demo Video
https://www.youtube.com/watch?v=6Gla6EM8-oA

## Link to Personal Website
https://jaxonyue.netlify.app

## Screenshots of Personal Website
![Website](/uploads/feaf01ba7288426e0fc4e8ea48b68eb5/Website.png)

## Screenshot of Netlify Deployment
![Netlify](/uploads/83c0dfa4683b79e62fcd4e0d3e099815/Netlify.png)

## Overview
* This repository includes the components for **Individual Project 1 - Continuous Delivery of Personal Website**

## Goal
* Website build with **Zola**
* GitLab workflow to build and deploy site on push
* Hosted on Netlify

## Key Steps
* Clone the repository
* Install Zola using the instructions here: https://www.getzola.org/documentation/getting-started/installation/
* Install your theme by choosing one from the Zola theme gallery: https://www.getzola.org/themes/, and then following the instructions for that theme. I chose the Hephaestus theme.
* Customize the theme to fit the needs of the site. I added two sections: "Education" and "Projects". In the "Education" section, I added my undergraduate and graduate education. In the "Projects" section, I added the links to my Gitlab repositories for the mini projects in IDS 721.
* Ensure that the runner can access the theme by adding the following to the .gitmodules file:
```
[submodule "themes/hephaestus"]
	path = themes/hephaestus
	url = https://github.com/BConquest/hephaestus.git
```
* Run `zola build` and then `zola serve` to build the site locally and check that it looks as expected
* Set up the GitLab CI/CD pipeline to build the site and deploy it to GitLab pages. I used the following .gitlab-ci.yml file:
```
stages:
  - deploy

default:
  image: debian:stable-slim
  tags:
    - docker

variables:
  # The runner will be able to pull your Zola theme when the strategy is
  # set to "recursive".
  GIT_SUBMODULE_STRATEGY: "recursive"

  # If you don't set a version here, your site will be built with the latest
  # version of Zola available in GitHub releases.
  # Use the semver (x.y.z) format to specify a version. For example: "0.17.2" or "0.18.0".
  ZOLA_VERSION:
    description: "The version of Zola used to build the site."
    value: ""

pages:
  stage: deploy
  script:
    - |
      apt-get update --assume-yes && apt-get install --assume-yes --no-install-recommends wget ca-certificates
      if [ $ZOLA_VERSION ]; then
        zola_url="https://github.com/getzola/zola/releases/download/v$ZOLA_VERSION/zola-v$ZOLA_VERSION-x86_64-unknown-linux-gnu.tar.gz"
        if ! wget --quiet --spider $zola_url; then
          echo "A Zola release with the specified version could not be found.";
          exit 1;
        fi
      else
        github_api_url="https://api.github.com/repos/getzola/zola/releases/latest"
        zola_url=$(
          wget --output-document - $github_api_url |
          grep "browser_download_url.*linux-gnu.tar.gz" |
          cut --delimiter : --fields 2,3 |
          tr --delete "\" "
        )
      fi
      wget $zola_url
      tar -xzf *.tar.gz
      ./zola build

  artifacts:
    paths:
      # This is the directory whose contents will be deployed to the GitLab Pages
      # server.
      # GitLab Pages expects a directory with this name by default.
      - public

  rules:
    # This rule makes it so that your website is published and updated only when
    # you push to the default branch of your repository (e.g. "master" or "main").
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```
* Push the changes to the repository and check the GitLab CI/CD pipeline to ensure that the site is built and deployed to GitLab pages.
* Sign up for Netlify and connect the GitLab repository
* Set up the deployment settings in Netlify as follows:
  * Build command: `zola build`
  * Publish directory: `\public`
  * Environment variables: `ZOLA_VERSION` as the key and `0.13.0` as the value
* Your website should then be automatically deployed and you can find your link in the "Site Overview" section of the Netlify dashboard
* Any push to the repository will trigger the GitLab CI/CD pipeline to build and deploy the site to GitLab pages, and then Netlify will automatically deploy the site to the link

## References
* https://www.getzola.org/documentation/getting-started/installation/
* https://www.getzola.org/themes/
* https://www.getzola.org/documentation/deployment/netlify/