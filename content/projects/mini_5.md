+++
title = "IDS 721 Mini Project 5"

[extra]
image = "https://yt3.googleusercontent.com/R6P5skGdZJeM1bebvt3ILeU8k-9tiqE5T198RmBH8SoGXH2gk_Lk-45uZoq6X6pW4a4c9Sqn=s900-c-k-c0x00ffffff-no-rj"
link = "https://gitlab.com/dukeaiml/IDS721/ids-mini-proj-5"
technologies = ["Serverless", "Rust", "Lambda"]
+++

Mini Project 5 - IDS 721 Cloud Computing.