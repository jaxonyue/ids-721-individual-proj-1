+++
title = "IDS 721 Mini Project 2"

[extra]
image = "https://yt3.googleusercontent.com/R6P5skGdZJeM1bebvt3ILeU8k-9tiqE5T198RmBH8SoGXH2gk_Lk-45uZoq6X6pW4a4c9Sqn=s900-c-k-c0x00ffffff-no-rj"
link = "https://gitlab.com/jaxonyue/ids-721-mini-proj-2"
technologies = ["Cargo", "Lambda", "Rush"]
+++

Mini Project 2 - IDS 721 Cloud Computing.